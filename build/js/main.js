function sliders() {
    $('.main__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        dots: true,
    });
    var prslider = $('.product__slider');
    prslider.owlCarousel({
        nav: true,
        dots: false,
        slideTransition: 'linear',
        autoplayTimeout: 0,
        autoplaySpeed: 4000,
        autoplayHoverPause: true,
        stopOnHover: true,
        mouseDrag: true,
        slideBy: 1,
        responsive:
        {
            0: {
                items: 2,
                slideBy: 1,
                stagePadding: 10,
                center: false
            },
            560: {
                items: 2,
                stagePadding: 50,
                center: false
            },
            700: {
                items: 3,
                center: false
            },
            768: {
                items: 3,
                nav: false,
                autoplay: false,
                center: false
            },
            1024: {
                items: 5,
                nav: true,
                autoplayHoverPause: true, 
            }
        }       
    });
    if($(document).width() >= 1024){
        $('.product__slider .owl-item').on('hover', function(e){
            // $($(this).parents('.product__slider')).trigger('stop.owl.autoplay');
            $('.product__slider').trigger('stop.owl.autoplay');
        });
        var owl = $('.owl-carousel');
        owl.on('mouseeover', function (e) {
            var data = $(e.currentTarget).data('owl.carousel');
            if (data.settings.autoplay) {
                $(e.currentTarget).trigger('stop.owl.autoplay');
            }
        });
        $('.product__slider .owl-prev').on('click', function(){
            $($(this).parents('.product__slider')).trigger('stop.owl.autoplay');
        });
        $(window).on('scroll', function(e) {
            var wH = $(window).height();
            var wS = $(this).scrollTop();
            var hT1 = $('#recommend--banner .product__slider').offset().top;
            var hH1 = $('#recommend--banner .product__slider').outerHeight();
            if(wS > (hT1+hH1-wH)){
                $("#recommend--banner .product__slider").trigger('play.owl.autoplay');
            }
            $('.product__slider .owl-item').mouseleave(function(){
                $('#recommend--banner .product__slider').trigger('stop.owl.autoplay');
                $('.product__slider.owl-carousel .owl-stage-outer').removeClass('visibility');
            })
            var hT2 = $('.popular--banner .product__slider').offset().top;
            var hH2 = $('.popular--banner .product__slider').outerHeight();
            if(wS > (hT2+hH2-wH)){
                $(".popular--banner .product__slider").trigger('play.owl.autoplay');
            }
            var hT3 = $('.products--banner .product__slider').offset().top;
            var hH3 = $('.products--banner .product__slider').outerHeight();
            if(wS > (hT3+hH3-wH)){
                $(".products--banner .product__slider").trigger('play.owl.autoplay');
            }
            var hT4 = $('.popular--banner.pop--phone .product__slider').offset().top;
            var hH4 = $('.popular--banner.pop--phone .product__slider').outerHeight();
            if(wS > (hT4+hH4-wH)){
                $(".popular--banner.pop--phone .product__slider").trigger('play.owl.autoplay');
            }
            $('.product__slider .owl-item').on('mouseenter', function(){
                $('#recommend--banner .product__slider').trigger('stop.owl.autoplay');
                $('.product__slider.owl-carousel .owl-stage-outer').addClass('visibility');
            });
        });
        $('.product__slider .owl-item').mouseover(function(e){
            $($(this).parents('.product__slider')).trigger('stop.owl.autoplay');
        });
        $('.product__slider .owl-prev').on('click', function(){
            $($(this).parents('.product__slider')).trigger('stop.owl.autoplay');
        });
        $('.product__slider .owl-item').mouseleave(function(){
            $('.product__slider.owl-carousel .owl-stage-outer').removeClass('visibility');
        });
    }
    if($(document).width() <= 1024){
        $(window).on('resize', function(){
            $(".product__list").on('initialize.owl.carousel');
            $('.product__list').addClass('owl-carousel');
            $(".product__list").owlCarousel({
                items: 4,
                nav: false,
                dots: false,
                loop: true,
                responsive:
                    {
                        0: {
                            items: 2,
                            slideBy: 1,
                            stagePadding: 30
                        },
                        560: {
                            items: 2,
                        },
                        700: {
                            items: 3,
                            loop: true
                        },
                        768: {
                            items: 3,
                        },
                        1024: {
                            items: 4,
                            nav: false
                        }
                    }
            });
            $('#viewed-banner .product__list').addClass('owl-carousel');
            $('#viewed-banner .product__list').owlCarousel({
                items: 4,
                nav: false,
                dots: false,
                loop: true,
                lazyLoad: true,
                responsiveRefreshRate: 100,
                responsiveBaseWidth: window,
                responsive:
                {
                    0: {
                        items: 2,
                        slideBy: 2,
                        stagePadding: 30,
                    },
                    560: {
                        items: 2,
                    },
                    700: {
                        items: 3,
                    },
                    768: {
                        items: 3,
                    },
                    1024: {
                        items: 4,
                    }
                }
            });
            $('.news__list').addClass('owl-carousel');
            $(".news__list").owlCarousel({
                items: 4,
                nav: false,
                dots: false,
                loop: true,
                autoplay: false,
                responsive:
                    {
                        0: {
                            items: 1,
                            slideBy: 1,
                            stagePadding: 50
                        },
                        560: {
                            items: 2,
                        },
                        700: {
                            items: 2,
                            loop: true
                        },
                        768: {
                            items: 2,
                        },
                        1024: {
                            items: 2,
                            nav: false
                        }
                    }
            });
        })    
    }

    // slider card of product
    $('.detail_slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        fade: true,
        asNavFor: '.detail_slider-nav'
    });
    $('.detail_slider-nav').slick({
        autoplay: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.detail_slider-for',
        dots: false,
        arrows: true,
        focusOnSelect: true
    });

    $('.gallery_for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        fade: true,
        asNavFor: '.gallery_nav'
    });

    $('.gallery_nav').slick({
        autoplay: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.gallery_for',
        dots: false,
        arrows: true,
        vertical: true,
        focusOnSelect: true
    });
 
}

function openCatMenu(){
    // catalog menu
    $('.main-menu>a').mouseover(function(){
        $(this).next().show();
        $('.menu__shadow').fadeIn(500);
    });
    $('.main-menu.cat__submenu').mouseleave(function(){
        var s = $(this);
        s.children('.catalog__desktop-menu').fadeOut();
    });

    $('.main-menu').on('mouseleave', function(e){
        $('.menu__shadow').hide();
        // $('.catalog__desktop-menu').fadeOut();
    });

    var $menu = $(".cat__menu-list");
        $menu.menuAim({
            activate: activateSubmenu,
            deactivate: deactivateSubmenu
        });
        function activateSubmenu(row) {
            var $row = $(row);
                $row.find(">div").show();
        }
        function deactivateSubmenu(row) {
            var $row = $(row);
            $row.find(">div").hide();    
        }
}

function headerEvents(){
    if($(document).width() >= 1024){
        $(window).on('scroll', function(e) {
            var top = $(window).scrollTop();
            e.preventDefault();
            if (top > 150) {
                $('.h-fixed').addClass('fixed');
            } else {
                $('.h-fixed').removeClass('fixed');
            }
        });

        $(document).mouseup(function (e) {
            var container = $(".modal__area, .header__modal");
            var block = $('.modal__area, .header__modal');
            if (container.has(e.target).length === 0){    
              block.fadeOut();
              $('body').removeClass('modal-open');
              $('.menu__shadow').hide();
            }
        });
    }
    $('.open-h-modal').on('mouseover', function(e){
        e.preventDefault();
        var hmodal = $(this).attr('data-modal');
        $(hmodal).fadeIn();
        $('.menu__shadow').show();
    });
    $($('.open-h-modal').parent()).on('mouseleave', function(e){
        e.preventDefault();
        $(this).children().next().hide();
        $('.menu__shadow').hide();
    });
    $('.menu__more .more__dropdown').on('mouseleave', function(e){
        e.preventDefault();
        $(this).fadeOut();
    });
    $('.open-h-modal-click').on('click', function(e){
        var hmodal = $(this).attr('data-modal');
        if($(hmodal).is(':visible')){
            return false;
        }
        else{
            $(hmodal).fadeToggle();
            $('body').addClass('modal-open');
            e.preventDefault();
        }
    });
    $('.close__btn').on('click', function(e){
        $(this).parents('.header__modal').fadeOut();
        $(this).parents('.modal__area').fadeOut();
        $('.menu__shadow').hide();
        $('body').removeClass('modal-open');
    });
    $('.search-input').on('click', function (e) {
        $(this).parent().next().show();
        $('.menu__shadow').show();
    });
    $('.open--modal, .btn__cart').on('click', function(e){
        e.preventDefault();
        var modal = $(this).attr('data-modal');
        $(modal).fadeIn();
    });
    $('.modal__area .close, .menu__shadow').on('click', function(e){
        e.preventDefault();
        $(this).parents('.modal__area').fadeOut();
    });
    openCatMenu();

//    mobile
    $('.mob__menu_btn').on('click', function(e){
        e.preventDefault();
        $('.mobile_menu__dropdown').slideDown();
        $('.menu-shadow').hide();
    });
    $('.menu__close').on('click', function(e){
        e.preventDefault();
        $(this).parents('.mobile_menu__dropdown').slideUp();
    });
    $('.mobile__nav__cat > a').on('click', function(e){
        e.preventDefault();
        $(this).next('.mobile__cat__dropdown').slideToggle();
    });
    $('.mobile__cat__dropdown ul .mobile__submenu > a, .open-mob_screen').on('click', function(e){
        e.preventDefault();
        console.log('click');
        $(this).next('.mobile__subscreen').fadeIn();
    });
    $('.mobile__subscreen.__depth2 .mobile__back__btn').on('click', function(e){
        e.preventDefault();
        $(this).parents('.mobile__subscreen.__depth2').hide();
    });
    $('.mobile__subscreen.__depth3 .mobile__back__btn').on('click', function(e){
        e.preventDefault();
        $(this).parents('.mobile__subscreen.__depth3').hide();
        $(this).parents('.mobile__subscreen.__depth2').show();
    });

    // tabs login and register
    $('.log_reg__menu ul li a').on('click', function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        $(link).addClass('show').siblings('.log_reg__tab').removeClass('show');
        $(this).parent().addClass('active').siblings().removeClass('active');
    });
    
    // checkbox legal
    $('.legal-check .checkbox-input').on('change', function(e){
        if($("#checkbox-legal").is(':checked')){
            $('#tab2').addClass('legal');
            $('.decor__form .legal_entity').show();
        }
        else{
            $('#tab2').removeClass('legal'); 
            $('.decor__form .legal_entity').hide();
        }
    });
    $('#check-city').on('change', function(e){
        console.log('asd')
        if($("#check-city").is(':checked')){
            $('.input-text.city-input').show();
        }
        else{
            $('.input-text.city-input').hide(); 
        }
    });
}
function catPageMenu(){
    $('.inner-sub__link').on('click', function(){
        $(this).next('.cat-links-list').show();
        $('.inner-sub__link').hide();
    });
    $('.category--link').on('click', function(){
        $(this).next('ul').show();
        $('.category--link').hide();
        $('.cat-links-block').addClass('remove');
    });
}
function validateForms(){
    $('.form').validate({
        rules: {
            user_phone: {
                required: true,
            },
            user_email: {
                required: true,
                email: true
            },
            user_name: {
                required: true
            },
            user_password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                equalTo: 'user_password'
            },
            user_checker: {
                required: true,
                checked: true
            }
        },
        messages: {
            user_phone: {
                required: 'Обязательное поле'
            },
            user_email: {
                required: 'Обязательное поле',
                email: 'Пожалуйста, введите корректный адрес электронной почты'
            },
            user_name: {
                required: 'Обязательное поле'
            },
            user_password: {
                required: 'Обязательное поле',
                minlength: 'Пожалуйста, введите не менее 5 символов.'
            },
            confirm_password: {
                required: 'Обязательное поле'
            },
            user_checker: {
                required: 'Обязательное поле'
            }
        },
        onfocusout: function(element) {
            return $(element).valid();
        }

    })
}
function swapImages(sel){
    var handle=0;
    sel.each(function(){
        var box=$(this);
        box.hover(function(){
            var counter=0;
            clearInterval(handle);
            handle=setInterval(function () {
                counter++;
                if(counter>=box.find('img').length) counter=0; 
                box.find('img').removeClass('active').eq(counter).addClass('active');
            }, 1000);
        },function(){
            clearInterval(handle);
            box.find('img').removeClass('active').eq(0).addClass('active');
        });
    });
}

function deletedCartMsg(){
    $('#modal--addedCart').fadeOut();
}
function openTabs(){
    $('.log_reg__menu ul li a').on('click', function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        $(link).fadeIn().siblings('.log_reg__tab').hide();
        $(this).parent().addClass('active').siblings().removeClass('active');
        return false;
    });
    $('.detail__tab__header li a').on('click', function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        $(link).show().siblings('.detail__block_tab').hide();
        $(this).parent().addClass('active').siblings().removeClass('active');
    });
}
function backToTop(){
    if($(document).width() <= 768){
        if ($('.back-to-top').length) {
            var scrollTrigger = 100, // px
                backToTop = function () {
                    var scrollTop = $(window).scrollTop();
                    var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();
                    if (scrollTop > scrollTrigger) {
                        $('.back-to-top').addClass('show');
                    } else {
                        $('.back-to-top').removeClass('show');
                    }
                    if(scrollBottom < 300){
                        $('.back-to-top').addClass('foot');
                    }
                    else{
                        $('.back-to-top').removeClass('foot'); 
                    }
                };
            backToTop();
            $(window).on('scroll', function () {
                backToTop();
            });
            $('.back-to-top').on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        }
    }
}

// function startTimer(duration, display) {
//     var start = Date.now(),
//         diff,
//         hours,
//         minutes,
//         seconds;
//     function timer() {
//         console.log()
//         diff = duration - (((Date.now() - start) / 1000) | 0);

//         hours = (diff/3600) | 0;
//         minutes = (diff / 60) % 60| 0;
//         seconds = (diff % 60) | 0;

//         hours = hours < 24 ? "" + hours : hours;
//         minutes = minutes < 10 ? "0" + minutes : minutes;
//         seconds = seconds < 10 ? "0" + seconds : seconds;

//         display.textContent = hours + ":" + minutes + ":" + seconds; 

//         if (diff <= 0) {
//             start = Date.now() + 1000;
//         }
//     };
//     timer();
//     setInterval(timer, 1000);
// }

    function makeTimer() {
		var endTime = new Date("22 January 2020 9:00:00 GMT+05:00");			
        endTime = (Date.parse(endTime) / 1000);

        var now = new Date();
        now = (Date.parse(now) / 1000);

        var timeLeft = endTime - now;

        var days = Math.floor(timeLeft / 86400); 
        var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
        var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
        var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

        if (hours < "10") { hours = "0" + hours; }
        if (minutes < "10") { minutes = "0" + minutes; }
        if (seconds < "10") { seconds = "0" + seconds; }

        $("#days").html(days + ":" );
        $("#hours").html(hours + ":");
        $("#minutes").html(minutes + ":");
        $("#seconds").html(seconds);		
	}

	setInterval(function() { makeTimer(); }, 1000);
 // обратный счетчик
// window.onload = function(){
//     var deadline = $('.product__today_time').text(),
//     display = document.querySelector('.product__today_time');
//     startTimer(deadline, display);
// }
function filterEvents(){
    $('.check__link').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('checked');
        $('.btn__show_filter').fadeToggle();
    });
    
    var $amount = $('#amount'),
    $min = $('.year-range-field'),
    $max = $('.year-range-max'),
    $min_input = $('#min_year'),
    $max_input = $('#max_year'),
    $slider = $('#year-slider').slider({
    orientation: 'horizontal',
    animate: "fast",
    range: true,
    min: 1960,
    max: 2019,
    values: [ 1960, 2019 ],
    slide: function(event, ui) {
        adjust(ui.values[0], ui.values[1]);
    }
    });
    function adjust(min, max) {
    $min.html(min);
    $max.html(max);
    $min_input.val(min);
    $max_input.val(max);
    $slider.find('.ui-slider-handle:first-of-type').attr('data-content',min);
    $slider.find('.ui-slider-handle:last-of-type').attr('data-content',max);
    }
    var min = $slider.slider('values', 0);
    var max = $slider.slider('values', 1);
    adjust(min, max);

    $('.view__control ._list').on('click', function(e){
        e.preventDefault();
        $('.product__list').addClass('list');
        $(this).parent().addClass('active');
        $(this).parents('li').siblings().children().removeClass('active');
    });
    $('.view__control ._table').on('click', function(e){
        e.preventDefault();
        $('.product__list').removeClass('list');
        $(this).parent().addClass('active');
        $(this).parents('li').siblings().children().removeClass('active');
    });
    $('.filter--item .show-all').on('click', function(e){
        e.preventDefault();
        $(this).prev().addClass('show');
        $(this).hide();
    })
}

function toolTip(){
    var cssSheet = document.styleSheets[0];
    var hoverIndex = cssSheet.insertRule('[data-tooltip]:hover:before {}', cssSheet.cssRules.length);
    var cssHover = cssSheet.cssRules[hoverIndex];				
    Array.from(document.querySelectorAll('[data-tooltip]')).forEach(function (item) {
      item.addEventListener('mousemove', function (e) {					
        if (this.dataset.tooltip == '') {
          cssHover.style.display = 'none';
          return;
        }
        cssHover.style.display = 'block';
        cssHover.style.left = (e.clientX + 10) + 'px';
        cssHover.style.top = (e.clientY + 10) + 'px';					
      });
    });
}

$(document).ready(function(){
    sliders();
    headerEvents();
    openTabs();
    catPageMenu();
    filterEvents();
    backToTop();
    validateForms();
    toolTip();
    //вы смотрели свернуть и раскрыть
    $('#viewed-banner .btn__view').on('click', function(){
        $('#viewed-banner').hide();
        let btn = $('#viewed-banner .btn__view');
        $(btn).text(function(i, v){
            return v === 'Свернуть' ? 'Раскрыть' : 'Свернуть'
        });
        $(btn).toggleClass('open');
    });
    $('#viewed-banner .btn__clean').on('click', function(){
        $('#viewed-banner .product__list').hide();
    });
    // change image of product each few seconds
    swapImages($(".product__block .product__img"));
    // popup add to cart
    $('.product__block .btn__cart').on('click', function(e){
        e.preventDefault();
        setTimeout(deletedCartMsg,1500)
    });
    $('.product__block .close__btn').on('click', function(e){
        e.preventDefault();
        $(this).parents('.product__block').hide();
    })
    $('.p-number .minus').click(function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.p-number .plus').click(function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });

    $('.about__text .show-all-text').on('click', function(e){
        e.preventDefault();
        $('.about__text').toggleClass('open');
        $(this).hide();
    });
    $('.footer__col.footer__nav').on('click', function(e){
        e.preventDefault();
        $(this).children('ul').slideToggle();
        $(this).toggleClass('open');
    });  
    $('.character .quest__icon').hover(function(){
        $(this).parents('span').next('.mini-info').toggleClass('visible');
    });
    $('.h_down_toggle').on('click', function(e){
        $(this).next().slideToggle();
    })
    $(".tel").inputmask('+7 (999) 999-99-99');
    easydropdown.all();
});

